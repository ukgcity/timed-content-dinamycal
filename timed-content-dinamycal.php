<?php
/**
 * @package Timed-content-dinamycal
 */
/*
Plugin Name: Timed content dinamycal
Plugin URI:
Description: Extend the plugin Timed-content.
Version: 1.0.0
Author: Yerbol O.
Author URI:
License: GPLv2 or later
Text Domain: timed-content-dinamycal
*/

$timed_contend_plugin_path = WP_PLUGIN_DIR.'/timed-content/timed-content.php';

if(file_exists($timed_contend_plugin_path)) {
	include_once($timed_contend_plugin_path);
}

if (!class_exists( "timedContentDinamycalPlugin" ) && class_exists('timedContentPlugin')) {
	define( "TIMED_CONTENT_DINAMYCAL_PLUGIN_URL", plugins_url() . '/timed-content-dinamycal' );

	class timedContentDinamycalPlugin extends timedContentPlugin {
		function rulesShowHTML_ext($atts, $content = "") {
			extract(shortcode_atts(array('id' => '0'), $atts));
			if (TIMED_CONTENT_RULE_TYPE != get_post_type($id)) return;

			$prefix = TIMED_CONTENT_RULE_POSTMETA_PREFIX;
			$args['action'] = get_post_meta($id, $prefix.'action', true);
			$args['freq'] = get_post_meta($id, $prefix.'frequency', true);
			$args['days_of_week'] = get_post_meta($id, $prefix.'weekly_days_of_week_to_repeat', true);
			if ($args['freq'] == 0) $args['interval_multiplier'] = get_post_meta($id, $prefix.'hourly_num_of_hours', true);
			if ($args['freq'] == 1) $args['interval_multiplier'] = get_post_meta($id, $prefix.'daily_num_of_days', true);
			if ($args['freq'] == 2) $args['interval_multiplier'] = get_post_meta($id, $prefix.'weekly_num_of_weeks', true);
			if ($args['freq'] == 3) $args['interval_multiplier'] = get_post_meta($id, $prefix.'monthly_num_of_months', true);
			if ($args['freq'] == 4) $args['interval_multiplier'] = get_post_meta($id, $prefix.'yearly_num_of_years', true);
			$args['timezone'] = get_post_meta( $id, $prefix . 'timezone', true );
			$args['instance_start'] = get_post_meta( $id, $prefix . 'instance_start', true );
			$args['instance_end'] = get_post_meta( $id, $prefix . 'instance_end', true );

			$timezone = $args['timezone'];
			$days_of_week = $args['days_of_week'];
			$interval_multiplier = $args['interval_multiplier'];
			$instance_start_date = $args['instance_start']['date'];
			$instance_start_time = $args['instance_start']['time'];
			$instance_end_date = $args['instance_end']['date'];
			$instance_end_time = $args['instance_end']['time'];

			$instance_start = strtotime( $this->__datetimeToEnglish( $instance_start_date, $instance_start_time ) . " " . $timezone );	// Beginning of first occurrence
			$instance_end = strtotime( $this->__datetimeToEnglish( $instance_end_date, $instance_end_time ) . " " . $timezone );    		// End of first occurrence
			$current = $instance_start;
			$end_current = $instance_end;

			$right_now_t = current_time('timestamp', 1);

			$active_periods = $this->getRulePeriodsById($id, false);

			$add_time = 0;
			switch($args['freq']) {
				case 0:
					$add_time = 60*60;// 1 hour
					break;
				case 1:
					$add_time = 24*60*60;// 1 day
					break;
				case 2:
					//$add_time = 7*24*60*60;// 1 week
					$add_time = $this->__getNextWeek($right_now_t, $interval_multiplier, $days_of_week) - $right_now_t;
					break;
				case 3:
					$add_time = $this->__getNextMonth($right_now_t, $right_now_t, 1) - $right_now_t;// 1 month
					break;
				case 4:
					$add_time = $this->__getNextYear($right_now_t, 1) - $right_now_t;// 1 year
					break;
			}

			$min_start = 1000000000000000000;
			$min_end   = 1000000000000000000;

			foreach ($active_periods as $period)  {
				if(($right_now_t <= $period['start']) && ($right_now_t + $add_time) >= $period['end']) {
					if($min_start > $period['start']) {
						$min_start = $period['start'];
						$min_end = $period['end'];
					}
				}
				if(($right_now_t >= $period['start']) && ($right_now_t) <= $period['end']) {
					$min_end = $period['end'];
				}
			}

			$start_action   = 'show';
			$end_action     = 'hide';
			$parameter_line = '';
			$style		= '';

			if($args['action'] == 1) {
				$start_action = 'show';
				$end_action   = 'hide';
			} else if($args['action'] == 0) {
				$start_action = 'hide';
				$end_action   = 'show';
			}

			if($min_start != 1000000000000000000) {
				$parameter_line = $start_action.'_'.($min_start - $right_now_t).'_'.$end_action.'_'.($min_end - $right_now_t);
			}

			if($min_start == 1000000000000000000 && $min_end != 1000000000000000000) {
				$parameter_line = $end_action.'_'.($min_end - $right_now_t);
			}

			if($min_start == 1000000000000000000 && $min_end == 1000000000000000000) {
				$style = 'display:none;';
			}

			return '<div class="timed-content-rule_'.$parameter_line.'" '.((strlen($style) > 0) ? ('style="'.$style.'"') : '').'>'.$content.'</div>';
		}
	}

}

function register_shortcodes(){
	if (class_exists("timedContentDinamycalPlugin")) {
		$timedContentDinamycalPluginInstance = new timedContentDinamycalPlugin();

		add_shortcode(TIMED_CONTENT_RULE_TAG, array(&$timedContentDinamycalPluginInstance, 'rulesShowHTML_ext'), 1);
	}
}

function addHeaderCode() {
	if (!is_admin()) {
		wp_enqueue_script('timed-content-dinamycal_js', TIMED_CONTENT_DINAMYCAL_PLUGIN_URL . '/js/timed-content-dinamycal.js', array( 'jquery' ), TIMED_CONTENT_VERSION );
	}
}

global $wp_version;

add_action('init', 'register_shortcodes');
add_action('wp_head', 'addHeaderCode');