jQuery(document).ready(function() {
	jQuery("div[class^='timed-content-rule'] ").each(function(index, element) {
		var clsname = jQuery(this).attr("class");
		var params = clsname.split("_");

		for (i = 1;i < params.length;i += 2)  {
			var action = params[i];
			var seconds = parseInt(params[i+1]);
			var ms = seconds*1000;

			if ((action == 'show') && (ms > 0)) {
				if(i == 1) jQuery(this).hide(0);
				var s = setTimeout( 
					function() {
						jQuery("div[class='" + clsname + "']").show(300);
					}, ms);
			}

			if ((action == 'hide') && (ms > 0)) {
				if(i == 1) jQuery(this).show(0);
				var h = setTimeout(
					function() {
						jQuery("div[class='" + clsname + "']").hide(300);
					}, ms);
			}
		}
	});
});